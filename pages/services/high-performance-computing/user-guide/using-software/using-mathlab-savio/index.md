---
title: Using MATLAB on Savio
keywords: high performance computing, MATLAB, Savio
last_updated: November 07, 2019
tags: [hpc, matlab]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/user-guide/using-software/using-matlab-savio/
folder: hpc
# DO NOT EDIT THE PREAMBLE BELOW THIS LINE #
search_content: This document describes how to use  MATLAB   a matrix based  technical computing language and environment for solving engineering and scientific problems  on the Savio high performance computing cluster at the University of California  Berkeley  Getting access to MATLAB on Savio As of summer 2019  you no longer need a MATLAB license to use MATLAB on Savio   Loading MATLAB To load MATLAB into your current software environment on Savio  at any shell prompt  enter  module load matlab  Viewing MATLAB Add Ons Once you are working interactively with MATLAB   you can then enter the ver command at MATLAB s interactive   prompt to view all of the MathWorks  Add Ons Toolboxes and Applications  currently provided on Savio  e g  ver   MATLAB Version  9 1 0 441655  R2016b      MATLAB                                            Version 9 1      R2016b  Simulink                                          Version 8 8      R2016b  Aerospace Blockset                                Version 3 18      R2016b  Aerospace Toolbox                                  Version 2 18      R2016b  Antenna Toolbox                                    Version 2 1      R2016b  Bioinformatics Toolbox                            Version 4 7    R2016b    Running MATLAB jobs in batch mode To run MATLAB in batch mode on Savio s compute nodes  enter the command sbatch followed by a single argument  the name of  or full path to  a job script file  Inside that job script file  at or near the end of that file  you will include the following  A command to load MATLAB into the software environment of the compute node  A command to run the MATLAB application  specifying that its input should come from a MATLAB script    m   file   If needed  that script file can  in turn  execute other scripts and functions  For more detailed information on using sbatch and job script files  see Running Your Jobs   Here s one example  which loads MATLAB  receives input from your script file  and saves selected output into an output log file   bin bash   Job name   SBATCH  job name matlabhelloworld example     Partition   SBATCH  partition savio     QoS   SBATCH  qos savio debug     Account   SBATCH  account account name     Wall clock limit   SBATCH  time 00 02 30     Command s  to run  module load matlab matlab  nosplash  nodesktop  r helloworld Running MATLAB jobs in batch mode with parallel computing code MATLAB is capable of distributing computations across multiple cores  allowing your scripts or functions to execute those computations  in parallel  That can significantly speed up some computational tasks  MATLAB on Savio includes a license for the Parallel Computing Toolkit  which facilitates parallel computation on one node  To use parallel computing features  you ll need to run MATLAB code that uses one or more of the parallel commands  functions  etc  available within the MATLAB programming language  Here s an example of a job script to run a MATLAB job that contains parallel computing code  in batch mode  on up to the full 20 cores offered by a compute node on the savio partition   bin bash   Job name   SBATCH  job name matlabhelloworld parallel example     Partition   SBATCH  partition savio     QoS   SBATCH  qos savio debug     Account   SBATCH  account account name     Request one node   SBATCH  nodes 1     Number of Processors per Node   SBATCH  ntasks per node 20     Wall clock limit   SBATCH  time 00 20 00     Command s  to run  module load matlab matlab  nodisplay  nosplash  nodesktop  r helloworld parallel To learn about the features of the MATLAB programming language that are capable of performing computations in parallel  e g  parfor   batch    spmd   and others  and how to use them in MATLAB scripts and functions  please see the MathWorks documentation  Introduction to Parallel Solutions or the brief discussion in the next section  In addition  some MATLAB products can intrinsically use parallel computing features without requiring coding  perhaps simply by setting a flag or preference  as described in Parallel Computing Support in MATLAB and Simulink Products   Controlling the number of cores used in MATLAB jobs There are a various ways to control the amount of parallelization in your MATLAB code  This discussion briefly outlines two ways  focusing on use of parfor for parallel loops and on threading  If you are running a job on the savio2 htc partition  using less than the entire node  or if you are running multiple jobs on a node that you have exclusive access to  you ll want to carefully control the number of cores in use by your MATLAB job s  Threading By default  a variety of MATLAB functions will use threading to do computations in parallel  To turn threading off  you can start MATLAB with the  singleCompThread flag  matlab  singleCompThread   Or  to control the number of threads used by your MATLAB process from within MATLAB  you use this syntax  in this case to use 5 cores  threads  maxNumCompThreads 5  You can also set the number of threads programmatically  often you ll do this based on environment variables that the SLURM scheduler sets  For example  if your submission script requested five cores per task  you can do this to set the number of threads to five  without having to hard code it into your MATLAB code  maxNumCompThreads str2num getenv SLURM CPUS PER TASK  Parallel looping   parallel pool MATLAB allows you to execute for loops in parallel via parfor   You ll first need to set up a pool of workers via parpool    which determines how many workers will be used to execute the iterations of the loop  MATLAB will set a default number of workers  the default is 12 workers  but if you want to use fewer workers  e g  to share the cores on a node across jobs  or more workers  e g  to use all the cores on a node you are using exclusively  you can tell parpool  how many workers to start  Here s an example of how to do this  in this case taking advantage of all the cores on a node  parpool str2num getenv SLURM CPUS ON NODE  Or to use as many cores as requested via  cpus per task  parpool str2num getenv SLURM CPUS PER TASK  As an alternative  if you want to change the default number of workers  e g  to 20 if you are always using the savio partition  you can modify your underlying parallel profile by running the following code one time only in MATLAB  c   parcluster  c NumWorkers   20  c saveProfile  Running MATLAB jobs on Savio s GPU nodes with parallel computing code To run MATLAB jobs that contain parallel computing code on Savio s Graphics Processing Unit GPU nodes  you ll need to write  adapt  or use MATLAB code that has been written for GPU access  To learn how to write or adapt MATLAB code that uses GPUs  please see GPU Programming in MATLAB   When submitting your job  make sure to request one or more GPUs for its use by including the  gres gpu x flag  where the value of   x   is 1   2   3   or 4   reflecting the number of GPUs requested  and also request two CPUs for every GPU requested within the job script file you include in your sbatch command or as an option in your srun command  For further details  please see the GPU example in the examples of job submissions with specific resource requirements   You can monitor GPU usage and availability on your node by entering nvidia smi from the UNIX command line  e g  in an interactive session  as described further below  If you requested a single GPU  you can set that up for use within MATLAB as follows  g   gpuDevice    now do GPU based operations To use multiple GPUs  each GPU node has 4 GPUs  in the same MATLAB job  you ll first need to use  gres gpu x in your job submission  where   x   is either 2   3   or 4   and then use parfor to start up multiple processes  as with this demo code  parfor i   1 gpuDeviceCount g   gpuDevice i    now do GPU based operations end Alternatively  assuming all four of a node s GPUs are available for your use  you could start four individual MATLAB jobs within your job script and set the device number via gpuDevice   to 1   2   3   and 4   respectively  within each of those individual MATLAB jobs  To check on the current usage  and hence availability  of each of the GPUs on your GPU node  you can use the nvidia smi command from the Linux shell within an interactive session on that GPU node  Near the end of that command s output  the  Processes  GPU Memory  table will list the GPUs currently in use  if any  For example  in a scenario where GPUs 0 and 1 are in use on your GPU node  you ll see something like the following   By implication from the output below  GPUs 2 and 3 are currently idle   not in use  and thus fully available   on this node      Processes  GPU Memory     GPU PID Type Process name Usage       0 32699 C  modules langs r 3 2 5 lib64 R bin exec R 729MiB     1 32710 C  modules langs r 3 2 5 lib64 R bin exec R 729MiB     Running MATLAB jobs across multiple Savio nodes The MATLAB Distributed Computing Server  DCS  toolbox  for MATLAB R2017b  and MATLAB Parallel Server  for MATLAB R2019a  allow one to run MATLAB jobs across multiple nodes  There are a few things you need to do to get it set up  but once you do  the parallel code you use in MATLAB will be the same as discussed in the previous section  One important limitation of this functionality is that the campus license only provides Savio with 32 MATLAB DCS licenses for MATLAB R2017b  This means that across all Savio users  only 32 MATLAB R2017b workers can be operating at once  Even if you are using all the licenses  32 workers is not much more than the number of cores on a node on most Savio partitions  However  note that it is possible to use multiple threads per worker  so one can potentially use more than 32 cores across multiple nodes for your MATLAB DCS using job  For information about how to use this  see Running MATLAB jobs across multiple Savio nodes   Running MATLAB interactively  for visualization  If you d like to use the full MATLAB GUI  graphical user interface  including for displaying graphics  we recommend you use our visualization node   Once you re up and running with a remote desktop session  you can simply start MATLAB  However  you can run MATLAB interactively without using the visualization node as discussed next  Running MATLAB interactively  command line  Here s how to run MATLAB interactively  so you can interact directly with this application at its command line  as contrasted with running it in batch mode  Step 1  Run an interactive shell To use MATLAB interactively on Savio s compute nodes  you can use the following example command  which uses the long form of each option to srun   to run a bash shell as a job on a compute node  That  in turn  should then let you launch MATLAB from that shell  on that compute node  and work directly at its command line   Note  the following command is only an example  and you ll need to substitute your own values for the sample  partition    qos    account   and  time values shown here see below for more details  srun  unbuffered  partition savio  qos savio normal  account ac scsguest  time 00 30 00 bash  i For more information on running interactive SLURM jobs on Savio  please see Running Your Jobs   Step 2  Load and run MATLAB from that shell Once you re working on a compute node  your shell prompt will change to something like this  where  n  followed by some number is the number of the compute node   myusername n0033   At that shell prompt  you can enter the following to load the MATLAB software module  module load matlab To start MATLAB for interactive use  via its command line  you can then enter matlab  nodisplay  nosplash Running MATLAB interactively  command line plus graphics  When you re running MATLAB on Savio in interactive mode  you can also set it up to display plots and similar graphical output on your computer  Here s how to do that  Step 1  Have X Window software installed and working on your computer You ll need to have X Window software installed and working on your computer  For instance  for Microsoft Windows  you can use Cygwin X   For Apple devices running macOS you can use XQuartz   Step 2  Log into Savio using the    Y   option to SSH To enable applications like MATLAB and others to interact with the X Window software on your computer  log into Savio using an ssh command that includes the  Y  trusted X11 forwarding  option  e g  ssh  Y myusername hpc brc berkeley edu Step 3  Run an interactive shell To use MATLAB interactively on Savio s compute nodes  you can use the following example command  which uses the long form of each option to srun   to run a bash shell as a job on a compute node  That  in turn  should then let you launch MATLAB from that shell  on that compute node  and work directly at its command line   Note  the following command is only an example  and you ll need to substitute your own values for the sample  partition    qos    account   and  time values shown here see below for more details  srun  pty  partition savio  qos savio normal  account ac scsguest  time 00 30 00 bash  i For more information on running interactive SLURM jobs on Savio  please see Running Your Jobs   Step 4  Load and run MATLAB from that shell Once you re working on a compute node  your shell prompt will change to something like this  where  n  followed by some number is the number of the compute node   myusername n0033   At that shell prompt  you can enter the following to load the MATLAB software module  module load matlab To start MATLAB for interactive use  via its command line  in a manner that also generates graphical output from that application  you can then enter  matlab Step 5   Optional  Generate a test plot Here s a quick way to try out MATLAB in graphical  interactive mode  by generating a test plot  The following statements  entered successively at MATLAB s interactive   prompt  will display a simple plot of squares of the integers 1 through 10  x   1   10  y   x 2  plot x y o  If all goes well  running the plot command  above  should result in output similar to the following being displayed in a window within your computer s X Window application 
---

<p>This document describes how to use<a href="https://www.r-project.org/" target="_blank">&nbsp;</a><a href="https://www.mathworks.com/products/matlab/" target="_blank">MATLAB</a>, a matrix-based, technical computing language and environment for solving engineering and scientific problems, on the Savio high-performance computing cluster at the University of California, Berkeley.</p>
<h3 id="Getting-access">Getting access to MATLAB on Savio</h3>
<p>As of summer 2019, you no longer need a MATLAB license to use MATLAB on Savio.</p>
<p>&nbsp;</p>
<h3 id="Loading-MATLAB">Loading MATLAB</h3>
<p>To load MATLAB into your current software environment on Savio, at any shell prompt, enter:</p>
<p><code>module load matlab</code></p>
<p>&nbsp;</p>
<h3 id="Viewing-Add-Ons">Viewing MATLAB Add-Ons</h3>
<p>Once you are <a href="#Interactive-CLI" class="toc-filter-processed">working interactively with MATLAB</a>, you can then enter the <code>ver</code>&nbsp;command at MATLAB's interactive <code>'&gt;&gt;'</code> prompt to view all of the MathWorks® Add-Ons&nbsp;(Toolboxes and Applications) currently provided on Savio; e.g.:</p>
<p><code>ver<br>
----------------------------------------------------------------------------------<br>
MATLAB Version: 9.1.0.441655 (R2016b)<br>
...<br>
----------------------------------------------------------------------------------<br>
MATLAB &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Version 9.1 &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (R2016b)<br>
Simulink &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Version 8.8 &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (R2016b)<br>
Aerospace Blockset &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Version 3.18 &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (R2016b)<br>
Aerospace Toolbox &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Version 2.18 &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (R2016b)<br>
Antenna Toolbox &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Version 2.1 &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (R2016b)<br>
Bioinformatics Toolbox &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Version 4.7 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (R2016b)<br>
...</code></p>
<h3 id="Batch-mode">Running MATLAB jobs in batch mode</h3>
<p>To run MATLAB in batch mode on Savio's compute nodes, enter the command <code>sbatch</code>&nbsp;followed by a single argument: the name of (or full path to) a job script file.</p>
<p>Inside that job script file, at or near the end of that file, you will include the following:</p>
<ul><li>A command to load MATLAB&nbsp;into the software environment of the compute node.</li>
<li>A&nbsp;command to run the MATLAB application, specifying that its input should come from a MATLAB script (<code>.m</code>) file. (If needed, that script file can, in turn, execute other scripts and functions.)</li>
</ul><p>For more detailed information on using <code>sbatch</code>&nbsp;and job script files, see <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/running-your-jobs"> Running Your Jobs</a>.</p>
<p>Here's one example, which loads MATLAB, receives input from your script file, and saves selected output into an output log file:</p>
<p><code>#!/bin/bash<br>
# Job name:<br>
#SBATCH --job-name=matlabhelloworld_example<br>
#<br>
# Partition:<br>
#SBATCH --partition=savio<br>
#<br>
# QoS:<br>
#SBATCH --qos=savio_debug<br>
#<br>
# Account:<br>
#SBATCH --account=account_name<br>
#<br>
# Wall clock limit:<br>
#SBATCH --time=00:02:30<br>
#<br>
## Command(s) to run:<br>
module load matlab<br>
matlab -nosplash -nodesktop -r&nbsp;helloworld</code></p>
<h3 id="Parallel-batch-mode">Running MATLAB jobs in batch mode with parallel computing code</h3>
<p>MATLAB is capable of distributing computations across multiple cores, allowing your scripts or functions to execute those computations "in parallel." That can significantly speed up some computational tasks.</p>
<p>MATLAB on Savio includes a license for the Parallel Computing Toolkit, which facilitates parallel computation on one node. To use parallel computing features, you'll need to run MATLAB code that uses one or more of the parallel commands, functions, etc. available within the MATLAB programming language.</p>
<p>Here's an example of a job script to run a MATLAB job that contains parallel computing code, in batch mode, on up to the full 20 cores offered by a compute node on the <code>savio</code>&nbsp;partition:</p>
<p><code>#!/bin/bash<br>
# Job name:<br>
#SBATCH --job-name=matlabhelloworld_parallel_example<br>
#<br>
# Partition:<br>
#SBATCH --partition=savio<br>
#<br>
# QoS:<br>
#SBATCH --qos=savio_debug<br>
#<br>
# Account:<br>
#SBATCH --account=account_name<br>
#<br>
# Request one node:<br>
#SBATCH --nodes=1<br>
#<br>
# Number of Processors per Node:<br>
#SBATCH --ntasks-per-node=20<br>
#<br>
# Wall clock limit:<br>
#SBATCH --time=00:20:00<br>
#<br>
## Command(s) to run:<br>
module load matlab<br>
matlab -nodisplay -nosplash -nodesktop -r&nbsp;helloworld_parallel</code></p>
<p>To learn about the features of the MATLAB programming language that are capable of performing computations in parallel (e.g., <code>parfor</code>, <code>batch()</code>, <code>spmd</code>, and others), and how to use them in MATLAB scripts and functions, please see the MathWorks documentation, <a href="https://www.mathworks.com/help/distcomp/introduction-to-parallel-solutions.html" target="_blank"> Introduction to Parallel Solutions</a>&nbsp;or the brief discussion in the next section.</p>
<p>In addition, some MATLAB products can intrinsically use parallel computing features without requiring coding, perhaps simply by setting a flag or preference, as described in <a href="https://www.mathworks.com/products/parallel-computing/parallel-support.html" target="_blank"> Parallel Computing Support in MATLAB and Simulink Products</a>.</p>
<h3 id="h.wcu9lk7a955a">Controlling the number of cores used in MATLAB jobs</h3>
<p>There are a various ways to control the amount of parallelization in your MATLAB code. This discussion briefly outlines two ways, focusing on use of <code>parfor</code>&nbsp;for parallel loops and on threading.</p>
<p>If you are running a job on the <code>savio2_htc</code> partition, using less than the entire node, or if you are running multiple jobs on a node that you have exclusive access to, you'll want to carefully control the number of cores in use by your MATLAB job(s).</p>
<h4 id="h.4jy1d0eybaw3">Threading</h4>
<p>By default, a variety of MATLAB functions will use threading to do computations in parallel. To turn threading off, you can start MATLAB with the <code>-singleCompThread</code>&nbsp;flag:</p>
<p><code>matlab -singleCompThread ...</code></p>
<p>Or, to control the number of threads used by your MATLAB process from within MATLAB, you use this syntax, in this case to use 5 cores (threads):</p>
<p><code>maxNumCompThreads(5);</code></p>
<p>You can also set the number of threads programmatically; often you'll do this based on environment variables that the SLURM scheduler sets. For example, if your submission script requested five cores per task, you can do this to set the number of threads to five, without having to hard-code it into your MATLAB code:</p>
<p><code>maxNumCompThreads(str2num(getenv('SLURM_CPUS_PER_TASK')));</code></p>
<h4 id="h.tlhoo3iisdxn">Parallel looping / parallel pool</h4>
<p>MATLAB allows you to execute for loops in parallel via <code>parfor</code>. You'll first need to set up a pool of workers via <code>parpool()</code>, which determines how many workers will be used to execute the iterations of the loop. MATLAB will set a default number of workers (the default is 12 workers), but if you want to use fewer workers (e.g., to share the cores on a node across jobs) or more workers (e.g., to use all the cores on a node you are using exclusively), you can tell <code>parpool()</code>&nbsp;how many workers to start.</p>
<p>Here's an example of how to do this, in this case taking advantage of all the cores on a node:</p>
<p><code>parpool(str2num(getenv('SLURM_CPUS_ON_NODE')));</code></p>
<p>Or to use as many cores as requested via <code>--cpus-per-task:</code></p>
<p><code>parpool(str2num(getenv('SLURM_CPUS_PER_TASK')));</code></p>
<p>As an alternative, if you want to change the default number of workers (e.g., to <code>20</code> if you are always using the <code>savio</code> partition), you can modify your underlying parallel profile by running the following code one-time only in MATLAB.</p>
<p><code>c = parcluster();<br>
c.NumWorkers = 20;<br>
c.saveProfile();</code></p>
<h3 id="GPUs">Running MATLAB jobs on Savio's GPU nodes&nbsp;with parallel computing code</h3>
<p>To run MATLAB jobs that contain parallel computing code on Savio's Graphics Processing Unit&nbsp;(GPU)&nbsp;nodes, you'll need to write, adapt, or use MATLAB code that has been written for GPU access. To learn how to write or adapt MATLAB code that uses GPUs, please see <a href="https://www.mathworks.com/company/newsletters/articles/gpu-programming-in-matlab.html" target="_blank"> GPU Programming in MATLAB</a>.</p>
<p>When submitting your job, make sure to request one or more GPUs for its use by including the <code>--gres=gpu:x</code> flag (where the value of '<code>x</code>' is <code>1</code>, <code>2</code>, <code>3</code>, or <code>4</code>, reflecting the number of GPUs requested), and also request two CPUs for every GPU requested,&nbsp;within the job script file you include in your <code>sbatch</code> command or as an option in your <code>srun</code> command. For further details, please see the GPU example in the <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/running-your-jobs#Job-submission-with-specific-resource-requirements" class="toc-filter-processed"> examples of job submissions with specific resource requirements</a>. You can monitor GPU usage and availability on your node by entering <code>nvidia-smi</code>&nbsp;from the UNIX command line (e.g., in an interactive session), as described further below.</p>
<p>If you requested a single GPU, you can set that up for use within MATLAB as follows:</p>
<p><code>g = gpuDevice();<br>
% now do GPU-based operations</code></p>
<p>To use multiple GPUs (each GPU node has 4 GPUs) in the same MATLAB job, you'll first need to use <code>--gres=gpu:x</code>&nbsp;in your job submission (where '<code>x</code>' is either <code>2</code>, <code>3</code>, or <code>4</code>) and then use <code>parfor</code>&nbsp;to start up multiple processes, as with this demo code:</p>
<p><code>parfor i = 1:gpuDeviceCount<br>
&nbsp;g = gpuDevice(i);<br>
&nbsp;% now do GPU-based operations<br>
end</code></p>
<p>Alternatively, assuming all four of a node's GPUs are available for your use, you could start four individual MATLAB jobs within your job script and set the device number,&nbsp;via <code>gpuDevice()</code>,&nbsp;to <code>1</code>, <code>2</code>, <code>3</code>, and <code>4</code>, respectively, within each of those individual MATLAB jobs.</p>
<p>To check on the current usage (and hence availability) of each of the GPUs on your GPU node, you can use the&nbsp;<code>nvidia-smi</code>&nbsp;command from the Linux shell within an interactive session on that GPU node. Near the end of that command's output, the "Processes: GPU Memory" table will list the GPUs currently in use, if any. For example, in a scenario where GPUs 0 and 1 are in use on your GPU node, you'll see something like the following. (By implication from the output below, GPUs 2 and 3 are currently idle - not in use, and thus fully available - on this node.)</p>
<p><code>+-----------------------------------------------------------------------------+<br>
| Processes: GPU Memory |<br>
| GPU PID Type Process name Usage |<br>
|=============================================================================|<br>
| 0 32699 C .../modules/langs/r/3.2.5/lib64/R/bin/exec/R 729MiB |<br>
| 1 32710 C .../modules/langs/r/3.2.5/lib64/R/bin/exec/R 729MiB |<br>
=============================================================================|</code></p>
<h3 id="DCS">Running MATLAB jobs across multiple Savio nodes</h3>
<p>The MATLAB Distributed Computing Server (DCS) toolbox (for MATLAB R2017b) and MATLAB Parallel Server (for MATLAB R2019a) allow one to run MATLAB jobs across multiple nodes. There are a few things you need to do to get it set up, but once you do, the parallel code you use in MATLAB will be the same as discussed in the previous section.</p>
<p>One important limitation of this functionality is that the campus license only provides Savio with 32 MATLAB DCS licenses for MATLAB R2017b. This means that across all Savio users, only 32 MATLAB R2017b workers can be operating at once. Even if you are using all the licenses, 32 workers is not much more than the number of cores on a node on most Savio partitions. However, note that it is possible to use multiple threads per worker, so one can potentially use more than 32 cores across multiple nodes for your MATLAB DCS-using job.</p>
<p>For information about how to use this, see <a href="{{ site.baseurl }}/services/high-performance-computing/using-matlab-savio/running-matlab-jobs-across-multiple-nodes">Running MATLAB jobs across multiple Savio nodes</a>.</p>
<h3 id="Interactive-CLI">Running MATLAB interactively (for visualization)</h3>
<p>If you'd like to use the full MATLAB GUI (graphical user interface), including for displaying graphics, we recommend you use our <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/using-brc-visualization-node-realvnc">visualization node</a>. Once you're up and running with a remote desktop session, you can simply start MATLAB.</p>
<p>However, you can run MATLAB interactively without using the visualization node as discussed next.</p>
<h3>Running MATLAB interactively (command line)</h3>
<p>Here's how to run MATLAB interactively, so you can interact directly with this application at its command line, as contrasted with running it in batch mode.</p>
<h4 id="h.1yl1fl7bnx73">Step 1. Run an interactive shell</h4>
<p>To use MATLAB interactively on Savio's compute nodes, you can use the following example command (which uses the long form of each option to <code>srun</code>) to run a bash shell as a job on a compute node. That, in turn, should then let you launch MATLAB from that shell, on that compute node, and work directly at its command line.</p>
<p>(Note: the following command is only an example, and you'll need to substitute your own values for the sample <code>--partition</code>,&nbsp;<code>--qos</code>, <code>--account</code>, and <code>--time</code>&nbsp;values shown here;&nbsp;see below for more details.)</p>
<p><code>srun --unbuffered --partition=savio --qos=savio_normal --account=ac_scsguest --time=00:30:00 bash -i</code></p>
<p>For more information on running interactive SLURM jobs on Savio, please see <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/running-your-jobs">Running Your Jobs</a>.</p>
<h4 id="h.qevddrdsbw1b">Step 2: Load and run MATLAB from that shell</h4>
<p>Once you're working on a compute node, your shell prompt will change to something like this (where 'n' followed by some number is the number of the compute node):</p>
<p><code>[myusername@n0033 ...]</code></p>
<p>At that shell prompt, you can enter the following to load the MATLAB software module:</p>
<p><code>module load matlab</code></p>
<p>To start MATLAB for interactive use, via its command line, you can then enter</p>
<p><code>matlab -nodisplay -nosplash</code></p>
<h3 id="Interactive-graphics">Running MATLAB interactively (command line plus graphics)</h3>
<p>When you're running MATLAB on Savio in interactive mode, you can also set it up to display plots and similar graphical output on your computer. Here's how to do that:</p>
<h4 id="h.ozwmk1xndsh0">Step 1. Have X Window&nbsp;software installed and working on your computer</h4>
<p>You'll need to have X Window&nbsp;software installed and working on your computer. For instance, for Microsoft Windows, you can use <a href="http://x.cygwin.com/" target="_blank">Cygwin/X</a> . For Apple devices running macOS,&nbsp;you can use <a href="https://www.xquartz.org/" target="_blank"> XQuartz</a>.</p>
<h4 id="h.p7p88stsw7tv">Step 2. Log into Savio using the '<code>-Y</code>' option to SSH</h4>
<p>To enable applications like MATLAB and others to interact with the X Window&nbsp;software on your computer, log into Savio using an ssh&nbsp;command that includes the <code>-Y</code>&nbsp;("trusted X11 forwarding") option; e.g.:</p>
<p><code>ssh -Y myusername@hpc.brc.berkeley.edu</code></p>
<h4 id="h.5nmou7izy1fx">Step 3. Run an interactive shell</h4>
<p>To use MATLAB interactively on Savio's compute nodes, you can use the following example command (which uses the long form of each option to <code>srun</code>) to run a bash shell as a job on a compute node. That, in turn, should then let you launch MATLAB from that shell, on that compute node, and work directly at its command line.</p>
<p>(Note: the following command is only an example, and you'll need to substitute your own values for the sample <code>--partition</code>,&nbsp;<code>--qos</code>, <code>--account</code>, and <code>--time</code>&nbsp;values shown here;&nbsp;see below for more details.)</p>
<p><code>srun --pty --partition=savio --qos=savio_normal --account=ac_scsguest &nbsp;--time=00:30:00 bash -i</code></p>
<p>For more information on running interactive SLURM jobs on Savio, please see <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/running-your-jobs">Running Your Jobs</a>.</p>
<h4 id="h.ecrj6nfpnvk0">Step 4: Load and run MATLAB from that shell</h4>
<p>Once you're working on a compute node, your shell prompt will change to something like this (where 'n' followed by some number is the number of the compute node):</p>
<p><code>[myusername@n0033 ...]</code></p>
<p>At that shell prompt, you can enter the following to load the MATLAB software module:</p>
<p><code>module load matlab</code></p>
<p>To start MATLAB for interactive use, via its command line, in a manner that also generates graphical output from that application, you can then enter:</p>
<p><code>matlab</code></p>
<h4 id="h.5c9n6aez6ur5">Step 5: (Optional) Generate a test plot</h4>
<p>Here's a quick way to try out MATLAB in graphical, interactive mode, by generating a test plot.</p>
<p>The following statements, entered successively at MATLAB's interactive <code>'&gt;&gt;'</code> prompt, will display a simple plot of squares of the integers 1 through 10:</p>
<p><code>x = 1 : 10;<br>
y = x.^2;<br>
plot(x,y,'-o')</code></p>
<p>If all goes well, running the <code>plot</code>&nbsp;command, above, should result in output similar to the following being displayed in a window within your computer's X Window&nbsp;application:</p>
<p><img alt="Sample plot of the squares of integers 1 through 10 generated by MATLAB" height="298" width="336" class="media-element file-default" data-delta="1" typeof="foaf:Image" src="{{ site.baseurl }}/images/MATLAB%20sample%20plot-squares%201-10.png"></p>
