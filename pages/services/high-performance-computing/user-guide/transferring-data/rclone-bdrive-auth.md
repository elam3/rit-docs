---
title: Authenticating rclone to Transferring Data Between Savio and Your UC Berkeley bDrive Account
keywords: high performance computing, berkeley research computing
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/user-guide/transferring-data/rclone-bdrive-auth
# DO NOT EDIT THE PREAMBLE BELOW THIS LINE #
search_content: Here are the steps for setting up a bDrive account for access via rclone from Savio  Research IT strongly recommends that rclone be used with a href https calnetweb berkeley edu calnet departments special purpose accounts spa  target blank Special Purpose Account  SPA  and not with the bDrive storage owned by  and accessible via  your personal CalNet ID  Separating this third party tool s access from the login you may use to store sensitive data or intellectual property  e g  papers and monographs in progress  FERPA protected student information  etc  is an effective way of safeguarding your files  Also  use of a SPA account conveniently enables desired access by current   and future   colleagues  successors  tech support personnel  et al  Note that you can set up access to multiple bDrive accounts by following the instructions below and entering unique names at the name  prompt  To set up a bDrive account for access  follow these instructions  1  Login to your Savio account and SSH to the dtn  where rclone is installed  bash 3 2  ssh paciorek hpc brc berkeley edu  paciorek ln003   ssh dtn 2  Configure rclone to access the bDrive account by setting up a new  remote   paciorek dtn   rclone config 2019 02 13 16 51 04 NOTICE  Config file  global home users paciorek config rclone rclone conf  not found   using defaults No remotes found   make a new one n  New remote s  Set configuration password q  Quit config n s q  n 3  Name the remote a unique name  you can use  bDrive  if only planning to access one bDrive account  but otherwise choose a unique name for each bDrive account  name  bDrive Type of storage to configure  Choose a number from below  or type in your own value  snip  9   Dropbox    dropbox  10   Encrypt Decrypt a remote    crypt  11   FTP Connection    ftp  12   Google Cloud Storage  this is not Google Drive     google cloud storage  13   Google Drive    drive  14   Google Photos    google photos  15   Hubic    hubic   snip  Storage  13   See help for drive backend at  https rclone org drive    4  Now answer a few questions   If speed is a concern  you may want to set up a Google Application Client Id  as suggested  Google Application Client Id Setting your own is recommended  See https rclone org drive making your own client id for how to create your own  If you leave this blank  it will use an internal key which is low performance  Enter a string value  Press Enter for the default   client id  Google Application Client Secret Setting your own is recommended  Enter a string value  Press Enter for the default   client secret  Scope that rclone should use when requesting access from drive  Enter a string value  Press Enter for the default   Choose a number from below  or type in your own value 1   Full access all files  excluding Application Data Folder     drive  2   Read only access to file metadata and file contents     drive readonly    Access to files created by rclone only  3   These are visible in the drive website    File authorization is revoked when the user deauthorizes the app     drive file    Allows read and write access to the Application Data folder  4   This is not visible in the drive website     drive appfolder    Allows read only access to file metadata but 5   does not allow any access to read or download file content     drive metadata readonly  scope  ID of the root folder Leave blank normally  Fill in to access  Computers  folders  see docs  or for rclone to use a non root folder as its starting point  Note that if this is blank  the first time rclone runs it will fill it in with the ID of the root folder  Enter a string value  Press Enter for the default   root folder id  Service Account Credentials JSON file path Leave blank normally  Needed only if you want use SA instead of interactive login  Enter a string value  Press Enter for the default   service account file  Edit advanced config   y n  y  Yes n  No y n  n 5  When asked to use  auto config  make sure to say  No  because you need to authenticate with Google via a browser on your local machine  e g  your laptop  Remote config Use auto config  Say Y if not sure Say N if you are working on a remote or headless machine y  Yes n  No y n  n 6  At this point  you ll be presented with a long URL that you should copy from your terminal window  and paste into your local machine s web browser  e g  paste from your terminal window into your laptop s web browser  After choosing the account through which to authenticate to Google Drive  you will be presented with a long string  which is the authentication token  7  Copy the long string from your browser and paste at the rclone config prompt in your terminal window  the string below is just an example   make sure to fill in your own  Enter verification code  6gyQGY9G63FNU3Fg3T Ym4PpyCawjJnnMaaaDXvBFuQGPbPshpKTyjpWc 8  You can now choose whether to configure this as a team drive  We ll enter  No  for a basic setup  Configure this as a team drive  y  Yes n  No y n  n 9  At this point  you ll see something like this  and you can accept the addition of the remote and quit out of the configuration   bDrive  type   drive token    access token ya29 a0Ae4lvC275d 5JYQfT94qxDigY9WTi04uhrw NG8O5hwTunE1X1M2pkXcVxMvevxCYqXoE WMYO6RT3PtWQKGpI4ukH8DqL 2sDFyXKpKMeVVEBp29 4OzGdAp9GSE0b6kWSFggCkwz90sO18ICPiRR0 KfPJwa38rc token type Bearer refresh token 1 06e5jBDwYQmAICgYIARAAGAYSNwF L9IrKgquwJfDNuyC7LdB8HchBlpjKNRc6TB vT6b5 cEtfg7Qw9qGAHBDMsw2QW74Vl3Etw expiry 2020 04 02T14 36 47 455756553 07 00    y  Yes this is OK e  Edit this remote d  Delete this remote y e d  y Current remotes  Name Type     bDrive bDrive e  Edit existing remote n  New remote d  Delete remote r  Rename remote c  Copy remote s  Set configuration password q  Quit config e n d r c s q  q Now you should be all set to copy files to from Savio and bDrive 
---

<p>Here are the steps for setting up a bDrive account for access via rclone from Savio.</p>
<p>Research IT strongly recommends that rclone be used with a <a
href="https://calnetweb.berkeley.edu/calnet-departments/special-purpose-accounts-spa"
target="_blank">Special Purpose Account</a> (SPA), and not with the bDrive storage owned by (and accessible via) your personal CalNet ID. Separating this third-party tool’s access from the login you may use to store sensitive data or intellectual property (e.g., papers and monographs in progress; FERPA-protected student information; etc.) is an effective way of safeguarding your files. Also, use of a SPA account conveniently enables desired access by current -- and future -- colleagues, successors, tech support personnel, et al.</p>
<p>Note that you can set up access to multiple bDrive accounts by following the instructions below and entering unique names at the <code>name&gt;</code> prompt.</p>
<p>To set up a bDrive account for access, follow these instructions:</p>
<p>1. Login to your Savio account and SSH to the dtn, where rclone is installed.</p>
<p><code>bash-3.2$ ssh paciorek@hpc.brc.berkeley.edu<br>
[paciorek@ln003 ~]$ ssh dtn</code></p>
<p>2. Configure rclone to access the bDrive account by setting up a new 'remote':</p>
<p><code>[paciorek@dtn ~]$ rclone config<br>
2019/02/13 16:51:04 NOTICE: Config file "/global/home/users/paciorek/.config/rclone/rclone.conf" not found - using defaults<br>
No remotes found - make a new one<br>
n) New remote<br>
s) Set configuration password<br>
q) Quit config<br>
n/s/q&gt; n</code></p>
<p>3. Name the remote a unique name; you can use "bDrive" if only planning to access one bDrive account, but otherwise choose a unique name for each bDrive account.</p>
<p><code>name&gt; bDrive<br>
Type of storage to configure.<br>
Choose a number from below, or type in your own value</code></p>
<p><code>[...snip...]<br>
 9 / Dropbox<br>
   \ "dropbox"<br>
10 / Encrypt/Decrypt a remote<br>
   \ "crypt"<br>
11 / FTP Connection<br>
   \ "ftp"<br>
12 / Google Cloud Storage (this is not Google Drive)<br>
   \ "google cloud storage"<br>
13 / Google Drive<br>
   \ "drive"<br>
14 / Google Photos<br>
   \ "google photos"<br>
15 / Hubic<br>
   \ "hubic"<br>
[...snip...]</code></p>
<p><code>Storage&gt; 13<br>
** See help for drive backend at: https://rclone.org/drive/ **</code></p>
<p>4. Now answer a few questions. (If speed is a concern, you may want
to set up a Google Application Client Id, as suggested.)<br>
<code>Google Application Client Id <br>
Setting your own is recommended.<br>
See https://rclone.org/drive/#making-your-own-client-id for how to create your own.<br>
If you leave this blank, it will use an internal key which is low performance.<br>
Enter a string value. Press Enter for the default ("").<br>
client_id><br>
Google Application Client Secret<br>
Setting your own is recommended.<br>
Enter a string value. Press Enter for the default ("").<br>
client_secret><br>
Scope that rclone should use when requesting access from drive.<br>
Enter a string value. Press Enter for the default (""). <br>
Choose a number from below, or type in your own value<br>
 1 / Full access all files, excluding Application Data Folder. <br>
   \ "drive"<br>
 2 / Read-only access to file metadata and file contents. <br>
   \ "drive.readonly"<br>
   / Access to files created by rclone only. <br>
 3 | These are visible in the drive website. <br>
   | File authorization is revoked when the user deauthorizes the app. <br>
   \ "drive.file"<br>
   / Allows read and write access to the Application Data folder. <br>
 4 | This is not visible in the drive website. <br>
   \ "drive.appfolder"<br>
   / Allows read-only access to file metadata but <br>
 5 | does not allow any access to read or download file content. <br>
   \ "drive.metadata.readonly"<br>
 scope><br>
ID of the root folder <br>
Leave blank normally. <br>
<br>
Fill in to access "Computers" folders (see docs), or for rclone to use <br>
a non root folder as its starting point. <br>
<br>
Note that if this is blank, the first time rclone runs it will fill it <br>
in with the ID of the root folder. <br>
<br>
Enter a string value. Press Enter for the default (""). <br>
root_folder_id><br>
Service Account Credentials JSON file path <br>
Leave blank normally. <br>
Needed only if you want use SA instead of interactive login. <br>
Enter a string value. Press Enter for the default (""). <br>
service_account_file> <br>
Edit advanced config? (y/n) <br>
y) Yes <br>
n) No <br>
y/n> n </code></p>
<p> 5. When asked to use 'auto config', make sure to say 'No', because
you need to authenticate with Google via a browser on your local
machine (e.g., your laptop.).</p>
<p><code>Remote config <br>
Use auto config?<br>
  Say Y if not sure <br>
  Say N if you are working on a remote or headless machine <br>
y) Yes <br>
n) No <br>
y/n> n </code></p>
<p>6. At this point, you’ll be presented with a long URL that you
should copy from your terminal window, and paste into your local
machine’s web browser (e.g., paste from your terminal window into your
laptop’s web browser). After choosing the account through which to authenticate to Google Drive, you will be presented with a long string, which is the authentication token.</p>
<p>7. Copy the long string from your browser and paste at the rclone
config prompt in your terminal window (the string below is just an example - make sure to fill in your own:<br>
<code>Enter verification code> 6gyQGY9G63FNU3Fg3T_Ym4PpyCawjJnnMaaaDXvBFuQGPbPshpKTyjpWc</code></p>
<p>8. You can now choose whether to configure this as a team
drive. We'll enter 'No' for a basic setup.<br>
<code>Configure this as a team drive?<br>
y) Yes <br>
n) No <br>
y/n> n </code></p>
<p>9. At this point, you'll see something like this, and you can accept the addition of the remote and quit out of the configuration.
<code>[bDrive]<br>
type = drive <br>
token = {"access_token":"ya29.a0Ae4lvC275d-5JYQfT94qxDigY9WTi04uhrw_NG8O5hwTunE1X1M2pkXcVxMvevxCYqXoE_WMYO6RT3PtWQKGpI4ukH8DqL-2sDFyXKpKMeVVEBp29-4OzGdAp9GSE0b6kWSFggCkwz90sO18ICPiRR0__KfPJwa38rc","token_type":"Bearer","refresh_token":"1//06e5jBDwYQmAICgYIARAAGAYSNwF-L9IrKgquwJfDNuyC7LdB8HchBlpjKNRc6TB_vT6b5-cEtfg7Qw9qGAHBDMsw2QW74Vl3Etw","expiry":"2020-04-02T14:36:47.455756553-07:00"}<br>
--------------------<br>
y) Yes this is OK <br>
e) Edit this remote <br>
d) Delete this remote <br>
y/e/d> y <br>
Current remotes:<br>
Name Type<br>
==== ====<br>
bDrive bDrive<br>
e) Edit existing remote<br>
n) New remote<br>
d) Delete remote<br>
r) Rename remote<br>
c) Copy remote<br>
s) Set configuration password<br>
q) Quit config<br>
e/n/d/r/c/s/q&gt; q</code></p>
<p>Now you should be all set to copy files to/from Savio and bDrive.</p>
