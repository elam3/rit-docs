---
title: Using SFTP with the BRC Supercluster via FileZilla
keywords: high performance computing, berkeley research computing
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/user-guide/transferring-data/using-sftp-savio-filezilla
# DO NOT EDIT THE PREAMBLE BELOW THIS LINE #
search_content: This document describes how to use SFTP  via the open source FileZilla application  to transfer data between your computer and the  Berkeley Research Computing  BRC  supercluster  consisting of the Savio  Vector  and Cortex high performance computing clusters  at the University of California  Berkeley  If you don t already have FileZilla  download it from the FileZilla website and follow the installation instructions   Versions of FileZilla are available for Windows  Mac OS X  and Linux  Open FileZilla  From the  File  menu  select  Site Manager  In Site Manager  Click the New Site button  In the Host field  enter  dtn brc berkeley edu From the Protocol  menu  select  SFTP   SSH File Transfer Protocol In the Logon Type  menu  select  Interactive In the User  field  enter   Your actual username  Click the Connect button  When the Enter Password dialog appears  Switch out of FileZilla to the Google Authenticator application  Generate a one time password   See Logging In for details  Copy that one time password to the Clipboard  Switch back to the FileZilla application  Put your token pin followed by the one time password into the Password field  If there is a  Remember password until FileZilla is closed  checkbox   be sure to uncheck it    This is important    Click the OK button   If all goes well  you ll see your files in your home directory displayed in FileZilla s  Remote Site  pane  To upload files or folders from your computer to the BRC supercluster  Select one or more file s  or folder s  in FileZilla s  Local Site  pane  Either drag those items into FileZilla s  Remote Site  pane  or right click on a selected item  then select  Upload  from the contextual menu that appears  Every time that the Enter Password dialog appears  complete every sub step under step 5  above  To download files or folders from the BRC supercluster to your computer  Select one or more file s  or folder s  in FileZilla s  Remote Site  pane  Either drag those items into FileZilla s  Local Site  pane  or right click on a selected item  then select  Download  from the contextual menu that appears  Every time that the Enter Password dialog appears  complete every sub step under step 5  above 
---

<p>This document describes how to use SFTP (via the open source FileZilla application) to transfer data between your computer and the  Berkeley Research Computing (BRC) supercluster, consisting of the Savio, Vector, and Cortex high-performance computing clusters, at the University of California, Berkeley.
<!--break--></p>

<ol>
<li>If you don't already have FileZilla, download it from the <a href="https://filezilla-project.org" target="_blank">FileZilla website</a> and follow the installation instructions. (Versions of FileZilla are available for Windows, Mac OS X, and Linux.)</li>
<li>Open FileZilla.</li>
<li>From the "File" menu, select "Site Manager..."</li>
<li>In Site Manager:

<ul>
<li>Click the New Site button.</li>
<li>In the Host field, enter: <code>dtn.brc.berkeley.edu</code></li>
<li>From the Protocol: menu, select: <code>SFTP - SSH File Transfer Protocol</code></li>
<li>In the Logon Type: menu, select:  <code>Interactive</code></li>
<li>In the User: field, enter: (Your actual username)</li>
<li>Click the Connect button.</li>
</ul></li>
<li>When the Enter Password dialog appears:

<ul>
<li>Switch out of FileZilla to the Google Authenticator application.</li>
<li>Generate a one-time password. (See <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/logging-brc-clusters">Logging In</a> for details.)</li>
<li>Copy that one-time password to the Clipboard.</li>
<li>Switch back to the FileZilla application.</li>
<li>Put your token pin followed by the one-time password into the Password field.</li>
<li>If there is a "Remember password until FileZilla is closed" checkbox,  be sure to <strong>uncheck</strong> it. (<em>This is important.</em>)</li>
<li>Click the OK button. (If all goes well, you'll see your files in your home directory displayed in FileZilla's "Remote Site" pane.)</li>
</ul></li>
<li>To upload files or folders from your computer to the BRC supercluster:

<ul>
<li>Select one or more file(s) or folder(s) in FileZilla's "Local Site" pane.</li>
<li>Either drag those items into FileZilla's "Remote Site" pane, or right-click on a selected item, then select "Upload" from the contextual menu that appears.</li>
<li>Every time that the Enter Password dialog appears, complete <em>every</em> sub-step under step 5, above.</li>
</ul></li>
<li>To download files or folders from the BRC supercluster to your computer:

<ul>
<li>Select one or more file(s) or folder(s) in FileZilla's "Remote Site" pane.</li>
<li>Either drag those items into FileZilla's "Local Site" pane, or right-click on a selected item, then select "Download" from the contextual menu that appears.</li>
<li>Every time that the Enter Password dialog appears, complete <em>every</em> sub-step under step 5, above.</li>
</ul></li>
</ol>

