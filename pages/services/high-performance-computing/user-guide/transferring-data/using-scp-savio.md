---
title: Using SCP with the BRC Supercluster
keywords: high performance computing, berkeley research computing
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/user-guide/transferring-data/using-scp-savio
# DO NOT EDIT THE PREAMBLE BELOW THIS LINE #
search_content: This document describes how to use SCP to transfer data between your computer and the Berkeley Research Computing  BRC  supercluster  consisting of the Savio  Vector  and Cortex high performance computing clusters  at the University of California  Berkeley  This video walks through the process of using SCP  The following examples show how to transfer files using SCP via the scp command in a shell terminal window  on your own computer running Mac OS X  Linux  or Unix  To transfer a single file from your computer to your home directory on the BRC supercluster  substitute your actual username for myusername in the two places it appears below  scp myfile txt myusername dtn brc berkeley edu global home users myusername To transfer a folder and its contents from your computer to your home directory on the BRC supercluster  substitute your actual username for myusername in the two places it appears below  scp  r myfolder myusername dtn brc berkeley edu global home users myusername To transfer that same folder to the shared Scratch filesystem on Savio  substitute your actual username for myusername in the two places it appears below  scp  r myfolder myusername dtn brc berkeley edu global scratch myusername
---

<p>This document describes how to use SCP to transfer data between your computer and the Berkeley Research Computing (BRC) supercluster, consisting of the Savio, Vector, and Cortex high-performance computing clusters, at the University of California, Berkeley.</p>
<!--break--><p>
This video walks through the process of using SCP:</p>
<iframe width="560" height="315" src="https://www.youtube.com/embed/ScBho4OkyJI?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe><p>
The following examples show how to transfer files using SCP via the <kbd>scp</kbd> command in a shell/terminal window, on your own computer running Mac OS X, Linux, or Unix.</p>
<p>To <strong>transfer a single file</strong> from your computer to your home directory on the BRC supercluster (substitute your actual username for <kbd>myusername</kbd> in the two places it appears below):</p>
<pre>scp myfile.txt myusername@dtn.brc.berkeley.edu:/global/home/users/myusername</pre><p>
To <strong>transfer a folder</strong> and its contents from your computer to your home directory on the BRC supercluster (substitute your actual username for <kbd>myusername</kbd> in the two places it appears below):</p>
<pre>scp -r myfolder myusername@dtn.brc.berkeley.edu:/global/home/users/myusername</pre><p>
To transfer that same folder to the shared Scratch filesystem on Savio (substitute your actual username for <kbd>myusername</kbd> in the two places it appears below):</p>
<pre>scp -r myfolder myusername@dtn.brc.berkeley.edu:/global/scratch/myusername</pre>
