---
title: High Performance Computing
keywords: high performance computing, cluster overview, mission, sustainability model, people
last_updated: September 30, 2019
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/user-guide/
folder: hpc
# DO NOT EDIT THE PREAMBLE BELOW THIS LINE #
search_content: This provides an overview of how to use the Savio and CGRL clusters  For details please click on the links below or in the sidebar  Passwords   You ll need to generate and enter a one time password each time that you log in  You ll use an application called Google Authenticator to generate these passwords  which you can install and run on your smartphone and or tablet  For instructions on setting up and using Google Authenticator  see Logging into the BRC Clusters   Logging in   You ll use your favorite SSH client program to log into the cluster via hpc brc berkeley edu   E g  from the command line  where you ll substitute your actual BRC Cluster username for myusername     ssh myusername hpc brc berkeley edu  For more detailed information on logging in  see Logging into the BRC Clusters   File storage   Once you log in  you ll be in your home directory    global home users  myusername   with a 10 GB storage limit  If you have an account on the Savio or Vector clusters  you also have access to a personal scratch directory  through which you ll share global storage with other cluster users  Some users may also have access to a group directory  shared with collaborators  You can access all of this storage   your home  scratch  and  if relevant  group directories   from the BRC supercluster s login and data transfer nodes  as well as from your cluster s compute nodes  For more information on your available storage  please see our storage page   Running your jobs   When you log into a cluster  you ll land on one of several login nodes  Here you can edit scripts  compile programs etc  However  you should not be running any applications or tasks on the login nodes  which are shared with other cluster users  Instead  use the SLURM job scheduler to submit jobs that will be run on one or more of the cluster s many compute nodes  You ll use SLURM commands like sbatch to submit your jobs  sinfo to view their status  and scancel to cancel them  Whenever you run sbatch   you ll point it at a SLURM job script  a small file that specifies where and how you want to run your job on the cluster and what command s  you want your job to execute  If you need to run jobs interactively  there s also an srun command available  See Running Your Jobs for more detailed information on submitting and running your jobs via SLURM  as well as the charges  if any  your account may incur for running computational jobs  Accessing or installing software   Lots of software packages and tools are already built and provided for your use  on your cluster  You can list these and load unload them via Environment Module commands  By default  SLURM and Warewulf commands are already added to your path  starting out  For all other provided software  at a shell prompt  enter module list to see what you re currently accessing  module avail to see what additional software is available  and one or more module load modulename or module unload modulename commands to set up your environment  For more detailed information on accessing provided software via Environment Module commands   as well as on installing your own software  when needed   please see Accessing Software   Transferring data   To transfer data from other computers into   or out of   your various storage directories  you can use protocols and tools like SCP  STFP  FTPS  and Rsync  If you re transferring lots of data  the web based Globus Connect tool is typically your best choice  it can perform fast  reliable  unattended transfers  Whenever you transfer data  you ll need to connect to the BRC supercluster s dedicated Data Transfer Node  dtn brc berkeley edu   For more information on getting your data onto and off of Savio  please see Transferring Data   For more information For additional information  please see Frequently Asked Questions pages or our Trainings and Tutorials   For additional help  support  or information  please see Getting Help   
---

This provides an overview of how to use the Savio and CGRL clusters. For details please click on the links below or in the sidebar.

<table border="0">
<tbody>
<tr>
<td align="top" width="150"><img alt="Key icon - representing password entry" height="129" width="134" class="media-element file-default image-style-none" data-delta="3" typeof="foaf:Image" src="{{ site.baseurl }}/images/key.png"></td>
<td><strong>Passwords</strong>. You'll need to generate and enter a one-time password each time that you log in. You'll use an application called Google Authenticator to generate these passwords, which you can install and run on your smartphone and/or tablet. For instructions on setting up and using Google Authenticator, see <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/logging-brc-clusters" target="_blank">Logging into the BRC Clusters</a>.</td>
</tr>
</tbody></table>

<table border="0">
<tbody>
<tr>
<td align="top" width="150"><img alt="Monitor icon - representing act of logging in" height="162" width="134" class="media-element file-default image-style-none" data-delta="4" typeof="foaf:Image" src="{{ site.baseurl }}/images/monitor_2_134px.png"></td>
<td><strong>Logging in</strong>. You'll use&nbsp;your favorite SSH client program to log into the cluster via <kbd>hpc.brc.berkeley.edu</kbd>. E.g., from the command line (where you'll substitute your actual BRC Cluster username for <kbd>myusername</kbd>):<code>$ ssh myusername@hpc.brc.berkeley.edu</code>&nbsp;</td>

For more detailed information on logging in, see <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/logging-brc-clusters#Logging-in" target="_blank">Logging into the BRC Clusters</a>.
</tr>
</tbody></table>

<table border="0">
<tbody>
<tr>
<td align="top" width="150"><img alt="File folder icon - signifying file storage" height="132" width="134" class="media-element file-default image-style-none" data-delta="11" typeof="foaf:Image" src="{{ site.baseurl }}/images/folder-grey_2_134px.png"></td>
<td><strong>File storage</strong>. Once you log in, you'll be in your home directory (<kbd>/global/home/users/<em>myusername</em></kbd>), with a 10 GB storage limit. If you have an account on the Savio or Vector clusters, you also have access to a personal scratch directory, through which you'll share global storage with other cluster users. Some users may also have access to a group directory, shared with collaborators.&nbsp;

You can access all of this storage - your home, scratch, and (if relevant) group directories - from the BRC supercluster's&nbsp;login and data transfer nodes, as well as from your cluster's compute nodes. For more information on your available storage, please see our <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/storing-data" target="_blank">storage page</a>.
</td></tr>
</tbody></table>

<table border="0">
<tbody>
<tr>
<td align="top" width="150"><img alt="Server icon - signifying running your jobs" height="177" width="134" class="media-element file-default image-style-none" data-delta="12" typeof="foaf:Image" src="{{ site.baseurl }}/images/server_134px.png"></td>
<td><strong>Running your jobs</strong>. When you log into a cluster, you'll land on one of several login nodes. Here you can edit scripts, compile programs etc. However, you should not be running any applications or tasks on the login nodes, which are shared with other cluster users. Instead, use the SLURM job scheduler to submit jobs that will be run on one or more of the cluster's many compute nodes.&nbsp;

You'll use SLURM commands like <kbd>sbatch</kbd> to submit your jobs, <kbd>sinfo</kbd> to view their status, and <kbd>scancel</kbd> to cancel them. Whenever you run <kbd>sbatch</kbd>, you'll point it at a SLURM job script, a small file that specifies where and how you want to run your job on the cluster and what command(s) you want your job to execute. If you need to run jobs interactively, there's also an <kbd>srun</kbd> command available. See <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/running-your-jobs" target="_blank">Running Your Jobs</a> for more detailed information on submitting and running your jobs via SLURM, as well as the charges (if any) your account may incur for running computational jobs.</td>
</tr>
</tbody></table>

<table border="0">
<tbody>
<tr>
<td align="top" width="150"><img alt="CD icon - signifying accessing and installing software" height="134" width="134" class="media-element file-default image-style-none" data-delta="13" typeof="foaf:Image" src="{{ site.baseurl }}/images/etiquette_cd-rom_01_134px.png"></td>
<td><strong>Accessing or installing software</strong>. Lots of software packages and tools are already built and provided for your use, on your cluster. You can list these and load/unload them via Environment Module commands. By default, SLURM and Warewulf commands are already added to your path, starting out. For all other provided software, at a shell prompt, enter <kbd>module list</kbd> to see what you're currently accessing, <kbd>module avail</kbd> to see what additional software is available, and one or more <kbd>module load <em>modulename</em></kbd> or <kbd>module unload <em>modulename</em></kbd> commands to set up your environment.&nbsp;

For more detailed information on accessing provided software via Environment Module commands - as well as on installing your own software, when needed - please see <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/accessing-software" target="_blank">Accessing Software</a>.</td>
</tr>
</tbody></table>

<table border="0">
<tbody>
<tr>
<td align="top" width="150"><img alt="File transfer icon" height="147" width="134" class="media-element file-default image-style-none" data-delta="15" typeof="foaf:Image" src="{{ site.baseurl }}/images/Binary-File-Plain_transfer_134px.png"></td>
<td><strong>Transferring data</strong>. To transfer data from other computers into - or out of - your various storage directories, you can use protocols and tools like SCP, STFP, FTPS, and Rsync. If you're transferring lots of data, the web-based Globus Connect tool is typically your best choice: it can perform fast, reliable, unattended transfers. Whenever you transfer data, you'll need to connect to the BRC supercluster's dedicated Data Transfer Node, <kbd>dtn.brc.berkeley.edu</kbd>. For more information on getting your data onto and off of Savio, please see <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/transferring-data" target="_blank">Transferring Data</a>.&nbsp;</td>
</tr>
</tbody></table>

<strong>For more information</strong>

For additional information, please see <a href="{{ site.baseurl }}/services/high-performance-computing/getting-help/frequently-asked-questions" target="_blank">Frequently Asked Questions</a> pages or our <a href="{{ site.baseurl }}/services/high-performance-computing/getting-help/training-and-tutorials" target="_blank">Trainings and Tutorials</a>.

For additional help, support, or information, please see <a href="{{ site.baseurl }}/services/high-performance-computing/getting-help" target="_blank">Getting Help</a>.

&nbsp;
