---
title: Community Contributions Guide
keywords: contribution
last_updated: April 21, 2020
tags: [hpc]
sidebar: hpc_sidebar
permalink: contributing
folder: hpc
---

## Overview

This site is generated using <a href="https://jekyllrb.com/" target="_blank">Jekyll</a> and was forked from the <a href="https://github.com/tomjoht/documentation-theme-jekyll" target="_blank">Jekyll Documentation Theme</a>. It is hosted on <a href="https://gitlab.com/ucb-rit/rit-docs" target="_blank">GitLab</a> using <a href="https://about.gitlab.com/product/pages/" target="_blank">GitLab Pages</a>.

We welcome your help with to make these docs as accurate as possible. You can [submit an issue](https://gitlab.com/ucb-rit/rit-docs/-/issues) to our GitLab repository or even [fork it](https://gitlab.com/ucb-rit/rit-docs/-/forks), make your changes, and submit a [merge request](https://gitlab.com/ucb-rit/rit-docs/-/merge_requests).

Below you will find a guide for contributions to this site. This guide is consistently updated, so please check back here regularly for changes.

## Setting up a local copy of this site

You'll often want to work on changes locally before committing them to the `rit-docs-testing` site (see [Gitlab Branching / Merging Workflow](#gitlab-branching--merging-workflow)).

To set get this Jekyll site running locally, you'll have to get `gem` if you don't already have it and then install the gems `bundler` and `jekyll` via 
```
gem install bundler jekyll
```
`gem` comes with Ruby, and there are various ways of getting it but most recommend using a package manager such as homebrew on macOS. For macOS, see here: <a href="https://stackoverflow.com/questions/39381360/how-do-i-install-ruby-gems-on-mac" target="_blank">https://stackoverflow.com/questions/39381360/how-do-i-install-ruby-gems-on-mac</a>

Once you have `gem` and install `bundler` and `jekyll`, you'll need a local copy of the `rit-docs-testing` repo. 
```
git clone git@gitlab.com:ucb-rit/rit-docs-testing.git
```
`cd` into the root of the repo and run
```
bundle update
bundle exec jekyll serve --baseurl=''
```
The `--baseurl=''` is important as it ensure that the internal URLs are formed correctly when running locally. Point your browser to the "Server address".

## Adding Pages, Subdirectories and Posts

### Pages

To create a new page, add a new `.md` file somewhere in the `pages` directory. While the directory structure has no effect on URLs, it is good practice to keep the pages organized in subdirectories that correspond to the permalink structure and to name files so that they match the final part of the URL slug.

The page title, summary, tags, and everything below the front matter (see below) will make up the visible content of the page. Any `<h*>` tags that are used (e.g. the markdown for an `<h2>` tag is `##`) will become part of the Table of Contents at the top of the page below the title.

<a href="https://daringfireball.net/projects/markdown/" target="_blank">See here</a> for more info on writing markdown.

### Subdirectory Landing Pages

Subdirectory landing pages are special pages that have a landing page and serve as the base for additional pages. For example, `services/high-performance-computing/` is a subdirectory landing page which exists in the repos `pages` directory and contains a number of other pages. The `index.md` (or optionsally `index.html`) file in the `high-performance-computing` directory plays a special role as the landing page at the url `{{ site.url }}/rit-docs/services/high-performance-computing/`. If this file is not present, then navigating to that URL gives a 404 not found error even if there is a file in the directory with the permalink `services/high-performance-computing`.

**Important Note:** The `permalink` in the front matter (see below) of the `index.md` subdirectory landing page file must contain a trailing `/` or the landing page will not be found due to issues in the interaction between how GitLab Pages and Jekyll handle permalinks. For example, `permalink: services/high-performance-computing/`.

Also note that the `/services/high-performance-computing/status-and-announcements/` page is a somewhat strange case. Its permalink requires the trailing '/' even though it is not a subdirectory landing page in the sense defined above. However, as it is a "newsfeed" page, it serves as the base for post detail pages (e.g., `services/high-performance-computing/status-and-announcements/savio-back`).

### Front Matter

All pages and posts require a preamble. For example, the front matter for this page is

```
---
title: How to use this site
sidebar: hpc_sidebar
permalink: index.html
tags: [getting_started, creating_content]
keywords: hpc
summary: A brief introduction to using this HPC documentation site.
search: exclude
---
```
Note that the homepage gets the special permalink `index.html`, while other pages have the permalinks you would expect. For example, the permalink for [Getting an Account]({{ site.baseurl }}/services/high-performance-computing/getting-account) is `services/high-performance-computing/getting-account`.

Another difference is that this page is not included in search results.

**Required elements**:
* `title` - this will appear in a `<h1>` tag at the top of the page/post and is used by the "search..." field in the header of the site.
* `sidebar` - use `hpc_sidebar`.
* `permalink` - this determines the URL slug (everything after the first `/`). For pages, the final part should match the name of the file (excluding `.md`). It is also used by search.

**Optional elements**:
* `tags` - Appear at the bottom of pages and posts (see below). Also, used by search and in invisible `<meta>` tags which are useful for SEO purposes. Tags should be in brackets and comma-separated as in the example above.
* `keywords` - Used by search and in `<meta>` tags.
* `summary` - Appears beneath the title (as above) and used by search.
* `search` - Use `exclude` if you do not want the page/post to appear in search results.

For more info on the front matter, <a href="https://jekyllrb.com/docs/front-matter/" target="_blank">see here</a>.

### Links within page content

For external links, please use the following HTML snippet so that they open in a new browser tab:

```
<a href="https://your-link.com" target="_blank">This link opens in a new tab.</a>
```

For internal links, `.md` is fine but we need to use a bit of <a href="https://github.com/Shopify/liquid/wiki" target="_blank">Liquid</a> to ensure that the link works the same way in all environments (i.e. local, testing and production). For example, the link above to the Getting an Account page was created using:
```
[Getting an Account]({{ "{{ site.baseurl " }}}}/services/high-performance-computing/getting-account)
```
The key here is the small bit of Liquid templating `{{ "{{ site.baseurl " }}}}` which prepends the correct URL for a given environment.

**Warning**: By default, [`kramdown`](https://kramdown.gettalong.org/), the Markdown parser used by Jekyll, will not parse Markdown within `html` block elements. If desired, we can change this behavior by setting the following in `_config.yml` ([source](https://stackoverflow.com/questions/50069585/internal-links-not-working-for-me-in-jekyll), not tested):

```
kramdown:
  parse_block_html: true
```

### Adding Images

To add an image to the site, first upload it to the `images` folder in the root of the repo, in a subfolder if desired for organization. Keep in mind that images should be as small and low-quality as possible wihtout sacrificing fidelity on a typical monitor/phone. If your image is `my-img.jpg`, you can add it to a page via Markdown or HTML:

**Markdown:**

`![Alt text]({{ "{{ site.baseurl " }}}}/images/my-img.png)`

**HTML:**

`<img alt="Alt text" height="50" width="50" src="{{ "{{ site.baseurl " }}}}/images/my-img.jpg">`

Screen readers use the `alt` attribute (in Markdown, the text in `[]`) so including it is important for accessbility.

## Gitlab Branching / Merging Workflow

**Notation**: `rit-docs:master` is the `master` branch of the `rit-docs` repository.

### <a href="https://gitlab.com/ucb-rit/rit-docs" target="_blank">`rit-docs`</a> repository

This is where the actually live user-facing documentation site lives, so you should not make changes directly to this repository (other than via merge request from the `master` branch of `rit-docs-testing`. This repo has one branch, `master` which renders at <a href="https://ucb-rit.gitlab.io/rit-docs/" target="_blank">`https://ucb-rit.gitlab.io/rit-docs/`</a>.

### <a href="https://gitlab.com/ucb-rit/rit-docs-testing" target="_blank">`rit-docs-testing`</a> repository

All changes to the documentation site should start in the <a href="https://gitlab.com/ucb-rit/rit-docs-testing" target="_blank">`rit-docs-testing`</a> repository. This repo has two main branches:

* <a href="https://gitlab.com/ucb-rit/rit-docs-testing/tree/master" target="_blank">`rit-docs-testing:master`</a>: The `master` branch should always be identical to (or, for brief moments before merging, at most one commit ahead of) the `master` of the live site's repo <a href="https://gitlab.com/ucb-rit/rit-docs" target="_blank">`rit-docs`</a>. This branch is not actually rendered at <a href="https://ucb-rit.gitlab.io/rit-docs-testing/" target="_blank">`https://ucb-rit.gitlab.io/rit-docs-testing/`</a>, and any changes to it should be made with the intention of immediately merging into `rit-docs:master`.
* <a href="https://gitlab.com/ucb-rit/rit-docs-testing/tree/deploy" target="_blank">`rit-docs-testing:deploy`</a>: The `deploy` branch is the branch that is actually rendered at <a href="https://ucb-rit.gitlab.io/rit-docs-testing/" target="_blank">`https://ucb-rit.gitlab.io/rit-docs-testing/`</a>. It's main job is to hold the correct config values for the testing site, and as a testing environment for your bigger changes (see below).

### Small changes

When you only need to change a small amount of copy or make other changes that don't have a large visual impact to the site, you can edit the <a href="https://gitlab.com/ucb-rit/rit-docs-testing/tree/master" target="_blank">`rit-docs-testing:master`</a> branch directly. There are two options:

1. Edit the page via the Gitlab GUI. Be sure that you are on the correct branch (`master`) and please include a commit message briefly describing the change. After you make your edits, clicking the "Commit changes" button will open a new merge request. A user with appropriate permissions (possibly you) will then be able to merge the new commit into `rit-docs-testing:master` and then create a merge request into the live site's `rit-docs:master` branch (see below). **More info**: <a href="https://docs.gitlab.com/ee/user/project/repository/web_editor.html" target="_blank">https://docs.gitlab.com/ee/user/project/repository/web_editor.html</a> and <a href="https://docs.gitlab.com/ee/user/project/web_ide/" target="_blank">https://docs.gitlab.com/ee/user/project/web_ide/</a>.
2. Clone the `rit-docs-testing` repo locally, make changes to the `master` branch, and push directly to `rit-docs-testing`

### Bigger changes

If you have a larger number of changes or changes that you want to test in a production-like environment, you'll want to create a new branch off of `rit-docs-testing:master`. There are probably ways to do this via the Gitlab GUI, but in the examples below I assume you've cloned the `rit-docs-testing` to your local system and have both the `master` and the `deploy` branches. A possible workflow is as follows:

* Create a new branch off of `master`, e.g.
```
git clone git@gitlab.com:ucb-rit/rit-docs-testing.git
git checkout master
git checkout -b big-changes
```
* Make your changes and merge them into the `deploy` branch locally:
```
git checkout deploy
git pull origin deploy
git merge big-changes
```
* Push the local version of `deploy` to the remote `rit-docs-testing:deploy`:
```
git push origin deploy
```
* At this point the Gitlab CI/CD pipeline will run to render your changes on the live testing site. <a href="https://gitlab.com/ucb-rit/rit-docs-testing/pipelines" target="_blank">Check here to see the status.</a> Once the pipeline runs, go to <a href="https://ucb-rit.gitlab.io/rit-docs-testing/" target="_blank">`https://ucb-rit.gitlab.io/rit-docs-testing/`</a> and check that your changes look good.
* If everything looked good, update your local copy of `rit-docs-testing:master`, merge your local `big-changes` into `master`, and push to the remote `master` branch:
```
git checkout master
git pull origin master
git merge big-changes
git push origin master
```
* Create a merge request from `rit-docs-testing:master` to `rit-docs:master` (see below).

Why not push directly to `rit-docs:master` at this point? To do so takes a bit of extra setup of your local repo and we want to keep `rit-docs-testing:master` up-to-date with the live repo's `master` branch anyways. Moreover, the hope is that 'the push to `rit-docs-testing:master` and merge request to `rit-docs:master`' workflow will prevent merge conflicts.

### Merge requests into the `rit-docs:master` repo

Once changes are integrated into the `rit-docs-testing:master`, it's time to merge them into the live site.

After pushing to `rit-docs-testing:master`, create a <a href="https://gitlab.com/ucb-rit/rit-docs-testing/merge_requests/new" target="_blank">New Merge Request</a> from the `rit-docs-testing` repo. The "Source branch" box should already say `ucb-rit/rit-docs-testing` and select `master` in the "Select source branch" dropdown in the same box. The target project and branch should be `ucb-rit/rit-docs` and `master`. 

Click on "Compare branches and continue" and give your merge request a title and description indicating what changes you made. Finally, click "Submit merge request" and you should be taken to a new page **in the `rit-docs` repo** with a summary of the merge request. If you have permissions to do so, you can click "Merge" to finalize the changes, making them live on the user-facing documentation site.
